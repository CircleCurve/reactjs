class House {
    constructor(){
        this.name = "house";
        this.doors = 3 ;
        this.windows = 10;
        this.color = "blue";
        this.hello = function () {
            console.log("Hello, I am a "+this.name);
            console.log("I have "+ this.doors+ " doors");
            console.log("I also have "+ this.windows + " windows");
            console.log("Color is "+ this.color);
        }
    }
}